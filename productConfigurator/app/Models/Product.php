<?php

namespace App\Models;

use App\Models\Size;
use App\Models\ProductImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'price', 'description', 'size_id'];

    public function size(){
        return $this->hasMany(Size::class);
    }

    public function images(){
        return $this->hasMany(ProductImage::class);
    }

}
