<footer class="container-fluid custom-footer">
    <div class="container">
        <div class="row border-bottom">
            <div class="col-12 col-md-3 mt-5">
                
                <img src="/img/logof.png" class="img-fluid my-2" width="250px" alt="">
                <p class="text-white">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni optio eligendi adipisci molestias quis iusto neque ducimus minus iste officiis, laborum sit, voluptatem sapiente porro aspernatur repudiandae! Odit, ipsa consequuntur.</p>
                
            </div>
            <div class="col-12 col-md-3 mt-5">
                
                <h4 class="text-uppercase text-white font-weight-bold ml-4">Sitemap</h4>
                <ul class="my-4">
                    <a href=""><li class="text-white list-unstyled">chi siamo</li></a>
                    <a href=""><li class="text-white list-unstyled">Lavora con noi</li></a>
                    <a href=""><li class="text-white list-unstyled">Aiuto</li></a>
                    <a href=""><li class="text-white list-unstyled">Resi & Rimborsi</li></a>
                    <a href=""><li class="text-white list-unstyled">Tabelle Taglie</li></a>
                    <a href=""><li class="text-white list-unstyled">Consegne</li></a>
                    <a href=""><li class="text-white list-unstyled">Trova un negozio</li></a>       
                </ul>
                
            </div>
            <div class="col-12 col-md-6 mt-5">
                <h4 class="text-uppercase text-white font-weight-bold">Newsletter</h4>
                <p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci, delectus! Esse magnam minima perspiciatis dicta.</p>
                <form action="" class="input-group mb-3">
                    <input type="email" class="w-50 rounded-pill form-control-lg">
                    <a href="" class="btn btn-primary text-uppercase rounded-pill text-white py-2 ml-4 font-weight-bold">Iscriviti</a>
                </form>
            </div>
        </div>
        <div class="row justify-content-around pt-3 pb-3">
            <div class="col-12 col-md-8 d-inline-block">
                <p class="text-white lead">© 2021 Felice Nicolas Barcellona / All rights reserved</p>
            </div>
            <div class="col-12 col-md-4 d-inline-block text-right">
                <a class="ml-1" href=""><i class="fab fa-facebook-f text-white mx-1 fa-lg"></i></a>
                <a class="ml-1" href=""><i class="fab fa-instagram text-white mx-1 fa-lg"></i></a>
                <a class="ml-1" href=""><i class="fab fa-twitter text-white mx-1 fa-lg"></i></a>
                <a class="ml-1" href=""><i class="fab fa-linkedin-in text-white mx-1 fa-lg"></i></a>
            </div>
        </div> 
    </div>
</footer>