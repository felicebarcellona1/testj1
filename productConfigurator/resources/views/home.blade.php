@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-9">
            <div class="">
                <img id="#droppable" class="d-block w-100" src="/img/shoes-configurator /scarpa-sinistra/left_inner_8051124195013_6.png" alt="Second slide">
            </div>
            {{-- <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="/img/shoes-configurator /scarpa-sinistra/left_back_8051124195013_7.png" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img id="#droppable" class="d-block w-100" src="/img/shoes-configurator /scarpa-sinistra/left_inner_8051124195013_6.png" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/img/shoes-configurator /scarpa-sinistra/left_outer_8051124195013_5.png" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/img/shoes-configurator /scarpa-destra/right_back_8051124195013_3.png" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/img/shoes-configurator /scarpa-destra/right_inner_8051124195013_1.png" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/img/shoes-configurator /scarpa-destra/right_outer_8051124195013_2.png" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> --}}
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 bg-white">
            <div class="card-body">
                <h4 class="card-title font-weight-bold text-uppercase">Sneakers in pelle</h4>
                <h5 class="card-text lead">€ 450</h5>
                <div class="input-group my-3 w-50">
                    <select class="custom-select" id="inputGroupSelect01">
                        <option selected>Misura...</option>
                        <option value="1">39</option>
                        <option value="2">40</option>
                        <option value="3">41</option>
                        <option value="4">42</option>
                        <option value="5">43</option>
                        <option value="6">44</option>
                        
                    </select>
                </div>
                <h5 class="lead mt-4">DESCRIZIONE</h5>
                <p>Sneakers della linea Portofino in vitello nappato con scritta Dolce&Gabbana nel tallone:</p>
                <ul>
                    <li>Tomaia in vitello</li>
                    <li>Lacci piatti</li>
                    <li>Fussbett in vitello con etichetta logata</li>
                    <li>Suola in gomma con spoiler in cuoio e logo microiniezione</li>
                    <li>Fondo logato</li>
                    <li>Made in Italy</li>
                </ul>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Aggiungi al carrello
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="d-flex d-inline-block">
                <div class="mx-2">
                    <img id="draggable" src="/img/shoes-configurator /patch/AACCS202387881.png" class="border" width="200px" alt="">
                </div>
                <div class="mx-2">
                    <img id="draggable" src="/img/shoes-configurator /patch/AACCS00204380311-1.png" class="border" width="200px" alt="">
                </div>
                <div class="mx-2">
                    <img src="/img/shoes-configurator /patch/AACCS00204380311.png" class="border" width="200px" alt="">
                </div>
                <div class="mx-2">
                    <img src="/img/shoes-configurator /patch/ADECO02796B80303-1.png" class="border" width="200px" alt="">
                </div>
                <div class="mx-2">
                    <img src="/img/shoes-configurator /patch/ADECO02796B80303.png" class="border" width="200px" alt="">
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection
