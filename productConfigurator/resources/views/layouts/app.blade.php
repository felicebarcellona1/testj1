<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Shop</title>
    
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;1,100;1,200;1,300;1,400&display=swap" rel="stylesheet">
    
    {{-- font awesome --}}
    <script src="https://kit.fontawesome.com/f78e1f7f4b.js" crossorigin="anonymous"></script>
    
    {{-- Jquery UI --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
    <div id="app">
        
        <x-nav/>
        
        <main class="py-4">
            @yield('content')
        </main>
        
        <x-footer/>
        
    </div>

    <script>
        
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        });
        
        
        
        
        $( "#draggable" ).draggable();
        $( "#droppable" ).droppable({
            drop: function() {
                alert( "dropped" );
            }
        });
        

    </script>
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script> 
</body>
</html>
