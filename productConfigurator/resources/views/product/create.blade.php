@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col md-12">
            <div class="card">
                <div class="card-header">PRODOTTO</div>
                <div class="card-body">
                    <h3>{{$uniqueSecret}}</h3>
                    <form action="{{route('product.store')}}" method="POST">
                        @csrf
                        <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">
                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">MODELLO</label>
                            <div class="col-md-10">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-md-2 col-form-label text-md-right">PREZZO</label>
                            <div class="col-md-10">
                                <input id="price" type="number" class="form-control w-50" name="price" value="" autofocus> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="size" class="col-md-2 col-form-label text-md-right">MISURE</label>
                            <div class="col-md-10">
                                <select name="size" id="size">
                                    @foreach ($sizes as $size)
                                        <option value="{{$size->id}}"}}>{{$size->name}}</option>         
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-2 col-form-label text-md-right">DESCRIZIONE</label>
                            <div class="col-md-10">
                                <textarea name="description" id="description" class="form-control" cols="30" rows="10" autofocus></textarea>
                            </div>
                        </div>
                        <div class="form-group-row">
                            <label for="images" class="col-md-2 col-form-label">IMMAGINI</label>
                            <div class="col-12">
                                <div class="dropzone" id="drophere"></div>
                            </div>
                        </div>
                        <button class="btn btn-primary text-uppercase my-3 mx-3 float-right">crea</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection